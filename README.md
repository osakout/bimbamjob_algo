# INSTALL #

You need NodeJS to run this project

[NodeJS](https://nodejs.org/en/download/)

# Install project #

Run this command in source folder to install project

```
npm i
```

# Run Project #

To start project run this command

```
npm start
```

All instructions for the robot are in ./instructions.txt
