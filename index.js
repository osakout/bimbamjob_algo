const fs = require('fs');
let tailleRectangle;
let nbLoop;
let i = 0;
let content = fs.readFileSync("./instructions.txt", "utf8").toString().split("\n");

tailleRectangle = content[0];

content.shift();

nbLoop = content.length / 2;

if (content.length % 2 === 0) {
    while (i != nbLoop) {
        let robotPosition = content[0];
        let instructionRobot;

        robotPosition = robotPosition.split('')

        content.shift();

        instructionRobot = content[0];
        instructionRobot = instructionRobot.split('');

        instructionRobot.forEach(element => {
            switch (robotPosition[3]) {
                case 'S':
                    {
                        if (element === "D") {
                            robotPosition[3] = "W";
                        } else if (element === "G") {
                            robotPosition[3] = "E";
                        } else if (element === "A") {
                            if (parseInt(robotPosition[1]) - 1 <= parseInt(tailleRectangle[1]) && parseInt(robotPosition[1]) -1 >= 0)
                                robotPosition[1] = parseInt(robotPosition[1]) - 1;
                        }
                    }
                    break;
                case 'W':
                    {
                        if (element === "D") {
                            robotPosition[3] = "N";
                        } else if (element === "G") {
                            robotPosition[3] = "S";
                        } else if (element === "A") {
                            if (parseInt(robotPosition[0]) - 1 <= parseInt(tailleRectangle[0]) && parseInt(robotPosition[0]) -1 >= 0)
                                robotPosition[0] = parseInt(robotPosition[0]) - 1;
                        }
                    }
                    break;
                case 'N':
                    {
                        if (element === "D") {
                            robotPosition[3] = "E";
                        } else if (element === "G") {
                            robotPosition[3] = "W";
                        } else if (element === "A") {
                            if (parseInt(robotPosition[1]) + 1 <= parseInt(tailleRectangle[1]))
                                robotPosition[1] = parseInt(robotPosition[1]) + 1;
                        }
                    }
                    break;
                case 'E':
                    {
                        if (element === "D") {
                            robotPosition[3] = "S";
                        } else if (element === "G") {
                            robotPosition[3] = "N";
                        } else if (element === "A") {
                            if (parseInt(robotPosition[0]) + 1 <= parseInt(tailleRectangle[0]))
                                robotPosition[0] = parseInt(robotPosition[0]) + 1;
                        }
                    }
                    break;
                default:
                    break;
            }
        });

        console.log("\nPOSITION FINALE ROBOT")

        let result = robotPosition.splice(0, 2)
        robotPosition.shift()

        console.log(result.concat(robotPosition).join(''))
        
        content.shift();
        i++;
    }
}